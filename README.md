# Guide draft

XDG user directories

    sudo xbps-install xdg-user-dirs
    run: xdg-user-dirs-update

Make the folders .fonts .icons .themes

    mkdir ~/.fonts ~/.icons ~/.themes

Create folders (not need)
    
    mkdir ~/Videos ~/Documents ~/Downloads ~/Pictures ~/Movies

Install Xorg

    sudo xbps-install -S xorg-minimal xrdb xorg-fonts
    sudo xbps-install -S xf86-video-intel

Info: xorg-minimal = xorg-server xauth xinit xf86-input-libinput

        test: 
        xorg-video-drivers xorg-input-drivers xwininfo xdpyinfo xsetroot

Install intel graphics drivers

    sudo xbps-install -S mesa-dri vulkan-loader mesa-vulkan-intel intel-video-accel

Install networkmanager

    sudo xbps-install -S NetworkManager

Install Alsa

    sudo xbps-install -S alsa-utils alsa-plugins alsa-lib alsa-firmware

Install PulseAudio

    sudo xbps-install -S pulseaudio alsa-plugins-pulseaudio pulseaudio-utils pulsemixer pavucontrol

Install fonts

    sudo xbps-install -S nerd-fonts/
    noto-fonts-ttf liberation-fonts-ttf font-unifont-bdf font-awesome terminus-font dejavu-fonts-ttf ttf-ubuntu-font-family fonts-roboto-ttf

     https://github.com/romkatv/powerlevel10k/blob/master/font.md
     

Install I3

    sudo xbps-install -S i3 polybar dunst compton feh arandr xrandr rofi

Install terminal

    sudo xbps-install -S rxvt-unicode

Install recommended packages

    sudo xbps-install -S git wget gcal unzip neofetch tree ntfs-3g btop brightnessctl numlockx setxkbmap elogind xrdb

Install shell

    sudo xbps-install -S zsh zsh-autosuggestions zsh-completions zsh-history-substring-search zsh-syntax-highlighting
	sudo usermod --shell /bin/zsh "$USER"

Install appearance management

    sudo xbps-install -S lxappearance

File manager

    sudo xbps-install -S pcmanfm lf xarchiver

Bluetooth

    sudo xbps-install -S bluez blueman
    
Music and Movies

    sudo xpbs-install -S strawberry smplayer mpd mpc mpv ncmpcpp ffmpeg

Browser

    sudo xbps-install -S firefox
    
Text editor

    sudo xbps-install -S neovim pluma

Printer

    sudo xbps-install -S cups simple-scan

Qemu + Virt Manager

    sudo xbps-install -S qemu virt-manager libvirt polkit
    sudo usermod -a -G libvirt,kvm <my_username>

    sudo xbps-install qemu-kvm libvirt-clients libvirt-daemon-system (no)

Office

    sudo xbps-install -S libreoffice

USB drive

    sudo xbps-install -S gvfs gvfs-mtp gvfs-gphoto2

Programs that i need

    sudo xbps-install -S tmux zathura xclip yt-dlp thunderbird shotwell digikam acpi lm_sensors playerctl

services

    sudo ln -sf /etc/sv/dbus /var/service
    sudo ln -sf /etc/sv/elogind /var/service
    sudo ln -sf /etc/sv/polkitd /var/service
    sudo ln -s /etc/sv/libvirtd /var/service
    sudo ln -s /etc/sv/virtlockd /var/service
    sudo ln -s /etc/sv/virtlogd /var/service
    sudo ln -sv /etc/sv/bluetoothd /var/service   
    
If you want to use thunar
    
    Thunar thunar-volman thunar-archive-plugin thunar-media-tags-plugin 

### the file .xinitrc
    setxkbmap be
    exec dbus-run-session i3

### Update system
    xbps-install -Su

### remove orphan packages (-o) and remove old caches (-O)
    xbps-remove -oO

### Set time
    sudo date -s “YYYY-MM-DD HH:MM:SS”

### Change default shell

    find the available shells: cat /etc/shells
    Type: chsh and press enter
    type de new shell full path: /bin/zsh
    Reboot the system
    Find out the current shell name: ps -p $$
    or
    chsh -s /bin/zsh ## change bash to ksh #

##########################################################################################

configure networkmanager \
sudo xbps-install network-manager-applet
sudo ln -sv /etc/sv/NetworkManager /var/service

Applications \
sudo xbps-install -S mlocate sysstat tree sxiv viewnior linux-headers urxvt-perls udiskie